var express = require('express')
var app = express()

process.title = "myapp"

app.get('/', function (req,res){
	res.send('Olá Mundo')
})

app.listen(3000, function(){
	console.log('Exemplo rodando na porta 3000!')
})

module.exports = app;
